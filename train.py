import os
import time
import itertools
import random
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
from tqdm import tqdm
from torch.utils.data import DataLoader, random_split

from dataset import ImageDataset, unnorm
from model import Generator, Discriminator, CycleGAN, save_checkpoint


def set_seed(seed):
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)

if __name__ == "__main__":
    set_seed(10)
    torch.backends.cudnn.benchmark = True  # type: ignore
    torch.backends.cudnn.deterministic = True  # type: ignore
    device = "cuda" if torch.cuda.is_available() else "cpu"

    img_ds = ImageDataset("./data/monet_jpg/", "./data/photo_jpg/")
    img_dl = DataLoader(img_ds, batch_size=2, pin_memory=True)
    gan = CycleGAN(3, 3, 50, device)

    save_dict = {
        "epoch": 0,
        "gen_mtp": gan.gen_mtp.state_dict(),
        "gen_ptm": gan.gen_ptm.state_dict(),
        "desc_m": gan.desc_m.state_dict(),
        "desc_p": gan.desc_p.state_dict(),
        "optimizer_gen": gan.adam_gen.state_dict(),
        "optimizer_desc": gan.adam_desc.state_dict(),
    }

    save_checkpoint(save_dict, "ckpt/init.ckpt")

    gan.train(img_dl)

    plt.xlabel("Epochs")
    plt.ylabel("Losses")
    plt.plot(gan.gen_stats.losses, "r", label="Generator Loss")
    plt.plot(gan.desc_stats.losses, "b", label="Descriminator Loss")
    plt.legend()

    _, ax = plt.subplots(5, 2, figsize=(12, 12))
    for i in range(5):
        photo_img, _ = next(iter(img_dl))
        pred_monet = gan.gen_ptm(photo_img.to(device)).cpu().detach()
        photo_img = unnorm(photo_img)
        pred_monet = unnorm(pred_monet)

        ax[i, 0].imshow(photo_img[0].permute(1, 2, 0))
        ax[i, 1].imshow(pred_monet[0].permute(1, 2, 0))
        ax[i, 0].set_title("Input Photo")
        ax[i, 1].set_title("Monet-esque Photo")
        ax[i, 0].axis("off")
        ax[i, 1].axis("off")

    plt.show()
