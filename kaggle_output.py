import os
import shutil
import torch
from PIL import Image
from tqdm import tqdm
from torchvision import transforms
from torch.utils.data import DataLoader, Dataset

from model import CycleGAN
from dataset import unnorm

class PhotoDataset(Dataset):
    def __init__(self, photo_dir, size=(256, 256), normalize=True):
        super().__init__()
        self.photo_dir = photo_dir
        self.photo_idx = dict()
        if normalize:
            self.transform = transforms.Compose([
                transforms.Resize(size),
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
            ])
        else:
            self.transform = transforms.Compose([
                transforms.Resize(size),
                transforms.ToTensor()
            ])
        for i, fl in enumerate(os.listdir(self.photo_dir)):
            self.photo_idx[i] = fl

    def __getitem__(self, idx):
        photo_path = os.path.join(self.photo_dir, self.photo_idx[idx])
        photo_img = Image.open(photo_path)
        photo_img = self.transform(photo_img)
        return photo_img

    def __len__(self):
        return len(self.photo_idx.keys())

if __name__ == '__main__':
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    gan = CycleGAN(3, 3, 50, device)

    state_dict = torch.load('./ckpt/current.ckpt')
    gan.gen_ptm.load_state_dict(state_dict['gen_ptm'])
    gan.gen_ptm.eval()

    ph_ds = PhotoDataset('./data/photo_jpg/')
    ph_dl = DataLoader(ph_ds, batch_size=1, pin_memory=True)
    trans = transforms.ToPILImage()
    t = tqdm(ph_dl, leave=False, total=ph_dl.__len__())
    for i, photo in enumerate(t):
        with torch.no_grad():
            pred_monet = gan.gen_ptm(photo.to(device)).cpu().detach()
        pred_monet = unnorm(pred_monet)
        img = trans(pred_monet[0]).convert("RGB")
        img.save("./output/images/" + str(i+1) + ".jpg")

    shutil.make_archive("./output/images_zip", 'zip', "./output/images")
