import os
import numpy as np
import torchvision.transforms as T
import matplotlib.pyplot as plt
from PIL import Image
from torch.utils.data import Dataset, DataLoader


class ImageDataset(Dataset):
    def __init__(self, monet_dir, photo_dir, size=(256, 256)):
        super().__init__()
        self.monet_dir = monet_dir
        self.photo_dir = photo_dir
        self.monet_idx = {}
        self.photo_idx = {}

        self.transform = T.Compose([T. Resize(size), T.ToTensor(), T.Normalize((.5, .5, .5), (.5, .5, .5))])

        for i, path in enumerate(os.listdir(self.monet_dir)):
            self.monet_idx[i] = path
        for i, path in enumerate(os.listdir(self.photo_dir)):
            self.photo_idx[i] = path

    def __getitem__(self, index):
        rand_idx = int(np.random.uniform(0, len(self.monet_idx.keys())))
        photo_path = os.path.join(self.photo_dir, self.photo_idx[rand_idx])
        monet_path = os.path.join(self.monet_dir, self.monet_idx[index])
        photo_img = Image.open(photo_path)
        monet_img = Image.open(monet_path)
        photo_img = self.transform(photo_img)
        monet_img = self.transform(monet_img)
        return photo_img, monet_img

    def __len__(self):
        return min(len(self.monet_idx.keys()), len(self.photo_idx.keys()))


def unnorm(img, mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5]):
    for t, m, s in zip(img, mean, std):
        t.mul_(s).add_(s)

    return img


if __name__ == '__main__':
    img_ds = ImageDataset('./data/monet_jpg/', './data/photo_jpg/')
    img_dl = DataLoader(img_ds, batch_size=1, pin_memory=True)
    photo_img, monet_img = next(iter(img_dl))
    f = plt.figure(figsize=(8, 8))

    f.add_subplot(1, 2, 1)
    plt.title('Photo')
    photo_img = unnorm(photo_img)
    plt.imshow(photo_img[0].permute(1, 2, 0))

    f.add_subplot(1, 2, 2)
    plt.title('Monet')
    monet_img = unnorm(monet_img)
    plt.imshow(monet_img[0].permute(1, 2, 0))
    plt.show()
